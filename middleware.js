import { NextResponse } from "next/server";

export default function middleware(req){
    let verify = req.cookies.get('__ecom_token__')
    let url = req.url
    if(!verify && url.includes('/admin')) {
        return NextResponse.redirect("http://localhost:3000/signin")
    }
    if(verify && (url.includes('/signin') || url.includes('/register'))) {
        return NextResponse.redirect("http://localhost:3000/")
    }
}