import Head from "next/head";
import { useState } from "react";
import ProductItem from "../components/product/ProductItem";
import ProductListing from "../components/product/ProductListing";
import { getData } from "./../utils/fetchData";
const baseUrl = process.env.BASE_URL;
export default function Home({ initialState, result }) {
  const [products, setProducts] = useState(initialState);
  return (
    <>
      <div className="px-2">
        <Head>
          <title>Home Page</title>
        </Head>

        {/* <div className="/products">
          {result === 0 ? (
            <h3>No Product Found</h3>
          ) : (
            products.map((elm, i) => <ProductItem key={i} product={elm} />)
          )}
        </div> */}
        <div className="/products">
          {result === 0 ? (
            <h3>No Product Found</h3>
          ) : (
            <ProductListing data={initialState}/>
          )}
        </div>
      </div>
    </>
  );
}

export async function getStaticProps() {
  const res = await getData(`product`);
  //server side rendering
  return {
    props: {
      initialState: res.products,
      result: res.result,
    },
  };
}
