import Link from "next/link";
import { useContext } from "react";
import { DataContext } from "../../store/GlobalStore";
import { addToCart } from "../../store/Actions";
import Image from "next/image";

function ProductItem({ product }) {
  const { state, dispatch } = useContext(DataContext);

  const { cart } = state;

  const userLinks = () => {
    return (
      <>
        <Link href={`product/${product._id}`} className="btn btn-primary w-100 me-1 my-1">View
        </Link>
        <button
          className="btn btn-success w-100 my-1"
          disabled={product.inStock === 0 ? true : false}
          onClick={() => dispatch(addToCart(product, cart))}
        >
          Add to Cart
        </button>
      </>
    );
  };

  return (
    <>


     




      <div
        className="card col-auto p-2 shadow-sm"
        style={{ width: "550px", minHeight: "270px" }}
      >
        <div className="row g-0">
          <div className="col-md-5 d-flex align-items-center justify-content-center">
            <Image
              src={product.images[0].url}
              className="img-fluid rounded-start"
              alt="..."
              width="850"
              height="550"
            />
          </div>
          <div className="col-md-7">
            <div className="card-body">
              <h5 className="card-title">{product.title}</h5>
              <p className="card-text">
                {product.description.substring(0, 60)} ...
              </p>
              <div className=" mx-0">
                <h6 className="card-text text-danger col">
                  Rs. {product.price}
                </h6>
                {product.inStock > 0 ? (
                  <h6 className="card-text text-danger">
                    In Stock. {product.inStock}
                  </h6>
                ) : (
                  <h6 className="card-text">Out of stock</h6>
                )}
              </div>
              <p className="card-text">
                <small className="text-muted">Last updated 3 mins ago</small>
              </p>
            </div>
          </div>
          <div className="flex w-100 justify-center">
            {userLinks()}
          </div>
        </div>
      </div>
    </>
  );
}

export default ProductItem;
