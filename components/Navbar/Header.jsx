import { Fragment, useContext } from "react";
import { Popover, Transition } from "@headlessui/react";
import {
    ArrowPathIcon,
    Bars3Icon,
    BookmarkSquareIcon,
    CalendarIcon,
    ChartBarIcon,
    CursorArrowRaysIcon,
    LifebuoyIcon,
    PhoneIcon,
    PlayIcon,
    ShieldCheckIcon,
    Squares2X2Icon,
    XMarkIcon,
} from "@heroicons/react/24/outline";
import { ChevronDownIcon } from "@heroicons/react/20/solid";
import Link from "next/link";
import { DataContext } from "../../store/GlobalStore";
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import Image from "next/image";


const NavMenus = [
    {
        name: "Home",
        description:
            "Home",
        href: "/",
        icon: ChartBarIcon,
    },
    {
        name: "Products",
        description: "Products Description",
        href: "/products",
        icon: ArrowPathIcon,
    },
    // {
    //     name: "Security",
    //     description: "Your customers' data will be safe and secure.",
    //     href: "#",
    //     icon: ShieldCheckIcon,
    // },
    // {
    //     name: "Integrations",
    //     description: "Connect with third-party tools that you're already using.",
    //     href: "#",
    //     icon: Squares2X2Icon,
    // },
    // {
    //     name: "Automations",
    //     description:
    //         "Build strategic funnels that will drive your customers to convert",
    //     href: "#",
    //     icon: ArrowPathIcon,
    // },
];
export default function Header() {
    const { state, dispatch } = useContext(DataContext);
    const { auth, cart } = state;

    const isActive = (r) => {
        if (r === router.pathname) {
            return " active";
        } else {
            return "";
        }
    };
    
    const router = useRouter();
    const logOut = () => {
        Cookies.remove("refreshToken", {
            path: "api/auth/accessToken",
        });
        Cookies.remove("__ecom_token__");
        localStorage.removeItem("firstLogin");
        dispatch({ type: "AUTH", payload: {} });
        dispatch({ type: "NOTIFY", payload: { success: "Logged Out" } });
        return router.push({
            pathname: "/"
        })
    };

    const afterLogin = (auth) => {
        return (
            <div class="flex justify-center">
                <div>
                    <div class="dropdown relative">
                        <a
                            class=" dropdown-toggle px-6 
                      text-black
                      font-medium
                      text-sm
                      leading-tight
                      uppercase
                      rounded
                      transition
                      duration-150
                      ease-in-out
                      flex
                      items-center
                      whitespace-nowrap"
                            type="button"
                            id="dropdownMenuButton1"
                            data-bs-toggle="dropdown"
                            aria-expanded="false"
                        >
                            {/* Dropdown button */}
                            <Image
                                src={auth?.user.avatar}
                                alt="avatar"
                                style={{
                                    borderRadius: "50%",
                                    transform: "translateY(0px)",
                                    marginRight: "3px",
                                    objectFit: "covers",
                                }}
                                width='30'
                                height='30'
                            />
                            &nbsp;
                            <span className="hover:text-bg-indigo-700">

                                {auth?.user?.name}
                            </span>
                        </a>
                        <ul
                            class="dropdown-menu
                                p-0
                                min-w-max
                                absolute
                                bg-white
                                text-base
                                z-50
                                float-left
                                list-none
                                text-left
                                rounded-lg
                                shadow-lg
                                mt-1
                                hidden
                                m-0
                                bg-clip-padding
                                border-none"
                            aria-labelledby="dropdownMenuButton1"
                        >
                            <li className="hover:bg-slate-200 rounded-lg">
                                <Link
                                    href="/cart"
                                    passHref
                                    className="dropdown-item
                                        text-sm
                                        py-2
                                        px-4
                                        font-normal
                                        block
                                        w-full
                                        whitespace-nowrap
                                        bg-transparent
                                        text-gray-700
                                        hover:bg-gray-100"
                                    aria-current="page"
                                    aria-hidden="true"
                                >
                                    <i
                                        className="fas fa-cart-plus me-2"
                                        aria-hidden="true"
                                    ></i>{" "}
                                    Profile
                                </Link>
                            </li>
                            <li className="hover:bg-slate-200 rounded-lg">
                                <Link
                                    href="/admin"
                                    passHref
                                    className="dropdown-item
                                        text-sm
                                        py-2
                                        px-4
                                        font-normal
                                        block
                                        w-full
                                        whitespace-nowrap
                                        bg-transparent
                                        text-gray-700
                                        hover:bg-gray-100"
                                    aria-current="page"
                                    aria-hidden="true"
                                >
                                    <i
                                        className="fas fa-cart-plus me-2"
                                        aria-hidden="true"
                                    ></i>{" "}
                                    Admin
                                </Link>
                            </li>
                            <li className="hover:bg-slate-200 rounded-lg">
                                <a
                                    passHref
                                    className="dropdown-item
                                        text-sm
                                        py-2
                                        px-4
                                        font-normal
                                        block
                                        w-full
                                        whitespace-nowrap
                                        bg-transparent
                                        text-gray-700
                                        hover:bg-gray-100"
                                    aria-current="page"
                                    aria-hidden="true"
                                    type="button"
                                    onClick={logOut}
                                >
                                    <i
                                        className="fas fa-cart-plus me-2"
                                        aria-hidden="true"
                                    ></i>
                                    Log Out
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    };
    return (
        <Popover className="relative bg-white shadow-sm">
            {/* <div className="mx-auto max-w-7xl px-6"> */}
            <div className="mx-auto px-8">
                <div className="flex items-center justify-between py-3 md:justify-start md:space-x-10">
                    <div className="flex justify-start lg:w-0 lg:flex-1">
                        <Link href="/" passHref>
                            <span className="sr-only">My Company</span>
                            <Image
                                className="h-8 w-auto sm:h-10"
                                src=""
                                alt="logo"
                                width={5}
                                height={5}
                            />
                        </Link>
                    </div>
                    <div className="-my-2 -mr-2 md:hidden">
                        <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                            <span className="sr-only">Open menu</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </Popover.Button>
                    </div>
                    <Popover.Group as="nav" className="hidden space-x-10 md:flex">

                        {NavMenus.map((_elm, _ind) => (
                            <Link
                                passHref
                                key={_ind}
                                href={_elm.href}
                                className="text-base font-medium text-gray-500 hover:text-gray-900"
                            >
                                {_elm.name}
                            </Link>
                        ))}
                    </Popover.Group>
                    <div className="hidden items-center justify-end md:flex md:flex-1 lg:w-0">
                        <Link href="/cart" passHref className={"whitespace-nowrap text-base font-medium text-gray-500 hover:text-gray-900 me-4" + isActive("/cart")}>
                            <i
                                className="fas fa-cart-plus"
                                aria-hidden="true"
                                style={{ position: "relative" }}
                            >
                                <span
                                    style={{
                                        padding: "3px 6px",
                                        background: "#ed143dc2",
                                        borderRadius: "50%",
                                        position: "absolute",
                                        top: "-10px",
                                        right: "-10px",
                                        color: "#fff",
                                        fontSize: "12px",
                                    }}
                                >
                                    {cart.length}
                                </span>
                            </i>
                            &nbsp;Cart
                        </Link>
                        {Object.keys(auth).length === 0 ? (
                            <>
                                <Link
                                    href="/signin"
                                    passHref
                                    className={
                                        "whitespace-nowrap text-base font-medium text-gray-500 hover:text-gray-900" +
                                        isActive("/signin")
                                    }
                                >
                                    <i className="fas fa-user mr-1" aria-hidden="true"></i> Sign
                                    In
                                </Link>
                                <Link
                                    href="/register"
                                    passHref
                                    className={
                                        "ml-8 inline-flex items-center justify-center whitespace-nowrap rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700" +
                                        isActive("/signin")
                                    }
                                >
                                    <i className="fas fa-user mr-2" aria-hidden="true"></i> Sign
                                    up
                                </Link>
                            </>
                        ) : afterLogin(auth)}
                    </div>
                </div>
            </div>

            <Transition
                as={Fragment}
                enter="duration-200 ease-out"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="duration-100 ease-in"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
            >
                <Popover.Panel
                    focus
                    className="absolute inset-x-0 top-0 origin-top-right transform p-2 transition md:hidden z-10"
                >
                    <div className="divide-y-2 divide-gray-50 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5">
                        <div className="px-5 pt-5 pb-6">
                            <div className="flex items-center justify-between">
                                <div>
                                    <Image
                                        className="h-8 w-auto"
                                        src=""
                                        alt="Your Company"
                                        width={5}
                                        height={5}
                                    />
                                </div>
                                <div className="-mr-2">
                                    <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                                        <span className="sr-only">Close menu</span>
                                        <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                                    </Popover.Button>
                                </div>
                            </div>
                            <div className="mt-6">
                                <nav className="grid gap-y-8">
                                    {NavMenus.map((_elm, _ind) => (
                                        <Link
                                            passHref
                                            key={_ind}
                                            href={_elm.name}
                                            className="-m-3 flex items-center rounded-md p-3 hover:bg-gray-50"
                                            aria-hidden="true"
                                        >
                                            <_elm.icon
                                                className="h-6 w-6 flex-shrink-0 text-indigo-600"
                                                aria-hidden="true"
                                            />
                                            <span className="ml-3 text-base font-medium text-gray-900">
                                                {_elm.name}
                                            </span>
                                        </Link>
                                    ))}

                                </nav>
                            </div>
                        </div>
                        <div className="space-y-6 py-6 px-5">

                         
                                {Object.keys(auth).length === 0 ? (
                                    <p className="mt-6 text-center text-base font-medium text-gray-500">
                                        Existing customer?
                                        <Link
                                            href="/signin"
                                            passHref
                                            className={
                                                "text-indigo-600 hover:text-indigo-500" +
                                                isActive("/signin")
                                            }
                                        >
                                            <i className="fas fa-user" aria-hidden="true"></i> Sign In
                                        </Link>
                                    </p>
                                ) : (
                                    afterLogin(auth)
                                )}
                        </div>
                    </div>
                </Popover.Panel>
            </Transition>
        </Popover>
    );
}
