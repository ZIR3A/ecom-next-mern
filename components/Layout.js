import Header from "./Navbar/Header";
import Notify from "./Notify";

export default function Layout({ children }) {
  return (
    <div className="container-fluid p-0 m-0">
      <Header />
      <Notify />
      <div className="mt-3 container-fluid">
        {children}
      </div>
    </div>
  );
}
