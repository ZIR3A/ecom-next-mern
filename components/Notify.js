import { useContext } from "react";
import { toast, ToastContainer } from "react-toastify";
import { DataContext } from "../store/GlobalStore";
import Loading from "./Loading";
import Toast from "./Toast";

const Notify = () => {
  const { state, dispatch } = useContext(DataContext);
  const { notify } = state;

  return (
    <>
      {notify.loading && <Loading />}
      {/* {notify.error && (
        <Toast
          msg={{ msg: notify.error, title: "Error" }}
          handleShow={() => dispatch({ type: "NOTIFY", payload: {} })}
          bgColor="bg-danger"
        />
      )} */}
      {notify.error && (
        <div class="max-w-lg ms-auto fixed top-3 right-3">
          <div class="flex bg-red-100 rounded-lg p-4 mb-4 text-sm text-red-700 justify-aorund w-80" role="alert">
            <svg class="w-5 h-5 inline mr-3" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
            <div>
              <span class="font-medium">Error alert!</span> {notify?.error}
            </div>
            <button
              type="button"
              className="text-red-700 ms-auto font-bold"
              onClick={() => dispatch({ type: "NOTIFY", payload: {} })}
            >X</button>
          </div>
        </div>
      )}
      {/* {notify.success && (
        <Toast
          msg={{ msg: notify.success, title: "Success" }}
          handleShow={() => dispatch({ type: "NOTIFY", payload: {} })}
          bgColor="bg-success"
        />
      )} */}
      {notify.success && (
        <div class="max-w-lg ms-auto fixed top-3 right-3">
          <div class="flex bg-green-100 rounded-lg p-4 mb-4 text-sm text-green-700 justify-aorund w-80" role="alert">
            <svg class="w-5 h-5 inline mr-3" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
            <div>
              <span class="font-medium">Success alert!</span> {notify?.success}
            </div>
            <button
              type="button"
              className="text-blue-700 ms-auto font-bold"
              onClick={() => dispatch({ type: "NOTIFY", payload: {} })}
            >X</button>
          </div>
        </div>
      )}
    </>
  );
};

export default Notify;
