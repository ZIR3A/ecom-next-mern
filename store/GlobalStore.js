import { createContext, useReducer, useEffect } from "react";
import { getData } from "../utils/fetchData";
import reducers from "./Reducers";
import jwt from "jsonwebtoken";

export const DataContext = createContext();

export const DataProvider = ({ children }) => {
  const initialState = { notify: {}, auth: {}, cart: [] };
  const [state, dispatch] = useReducer(reducers, initialState);
  const { cart, notify} = state;

  useEffect(() => {
    const firstLogin = localStorage.getItem("firstLogin");
    if (firstLogin) {
      getData("auth/accessToken").then((res) => {
        if (res.err) return localStorage.removeItem("firstLogin");
        dispatch({
          type: "AUTH",
          payload: {
            token: res.access_token,
            user: res.user,
          },
        });
      });
    }
  }, []);

  useEffect(()=> {
    if(Object.keys(notify).length > 0) {
      setTimeout(()=> {
        dispatch({type: "NOTIFY", payload: {}})
      },4000)
    }
  },[notify])

  useEffect(() => {
    const __next__cart01__zirea = JSON.parse(
      localStorage.getItem("__next__cart01__zirea")
    );
    if (__next__cart01__zirea)
      dispatch({ type: "ADD_CART", payload: __next__cart01__zirea });
  }, []);
  useEffect(() => {
    localStorage.setItem("__next__cart01__zirea", JSON.stringify(cart));
  }, [cart]);

  return (
    <DataContext.Provider value={{ state, dispatch }}>
      {children}
    </DataContext.Provider>
  );
};
